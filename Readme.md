# How to Run

This is a step-by-step guide how to run the example:

## Installation

* The example is implemented in Java. A JRE (Java Runtime Environment) is not
   sufficient. After the installation you should be able to execute
   `java` and `javac` on the command line.

* The example run in Docker Containers. You need to install Docker
  Community Edition, see https://www.docker.com/community-edition/
  
* After installing Docker you should also be able to run
  `docker-compose`. If not, install https://docs.docker.com/compose/install/ 

* Use Ubuntu, not guaranteed to work on Windows.

## Build

In `./microservice-kafka` run `./mvnw clean package`. It will take a while but wait for the:

```

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
```

If it doesn't work the first time:

* Delete `settings.xml` in the directory `.m2` 

* Make sure the Kafka port 9092 and the HTTP port 8080 are
available.

* Skip the tests: `./mvnw clean package -D maven.test.skip=true`

* Use `sudo`

## Run the containers

Go to the directory `docker` and run `docker-compose build`. It will take a while but wait for the:

```
Successfully built af6e0b1495b4
Successfully tagged mskafka_apache:latest
```

Check the containers image with `docker images`

Start containers using `docker-compose up` or `docker-compose up -d` for starting them as daemons.


`docker ps -a`  also shows the terminated Docker containers. 

If one of the containers is not running, you can look at its logs using `docker logs mskafka_order_1`. The name of the container is given in the last column of the output of `docker ps`. Looking at the logs even works after the container has been terminated. If the log says that the container has been `killed`, you need to increase the RAM assigned to Docker to 4GB or more.


Check if the workflow works at to http://localhost:8080/ and enter an order.

You can terminate all containers using `docker-compose down`.


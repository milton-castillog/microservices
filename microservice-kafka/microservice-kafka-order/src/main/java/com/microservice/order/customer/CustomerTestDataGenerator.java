package com.microservice.order.customer;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CustomerTestDataGenerator {

	private final CustomerRepository customerRepository;

	@Autowired
	public CustomerTestDataGenerator(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	@PostConstruct
	public void generateTestData() {
		customerRepository
				.save(new Customer("Milton", "Castillo", "milton.castillo@alumnos.uneatlantico.es", "Calle vargas 100", "Santander"));
		customerRepository.save(new Customer("Usnavi", "Blackout", "usnavi@gmail.com", "Washington heights", "New York"));
	}

}
